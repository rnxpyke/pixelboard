-- Your SQL goes here

CREATE TABLE images (
    id SERIAL PRIMARY KEY,
    created TIMESTAMP NOT NULL DEFAULT NOW()
)