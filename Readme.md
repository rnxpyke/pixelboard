# Pixelboard

A full-stack Rust Image Viewer. The project is split in three cargo workspaces
- frontend 
- backend 
- database

Backend uses Rocket as a Webserver and Diesel as a ORM with a Postgres Database.
Frontend uses yew compiled to WASM.


## Setting up

Run the needed database and its migrations.

### Setting up docker

```bash
pacman -S docker

# depending on wether you're already in the docker group
# you may have to log out and in again
usermod -a -G docker $USERNAME
systemctl enable --now docker
```

### Install diesel

```bash
pacman -S postgresql #needed for .so objects

cargo install diesel_cli --no-default-features --features postgres
```

### Setting up the database

```bash
# Set the database settings
export PASS=yourstrongpasswordhere
export IP=172.17.0.2
export NAME=pixelboard_db

# create the docker image for the database
docker create --name pixelgres \
    --ip $IP \
    -e POSTGRES_PASSWORD=$PASS \
    -e POSTGRES_DB=$NAME \
    postgres

# you can stop/start the database now with docker (start/stop)
docker start pixelgres

export DATABASE_URL="postgres://postgres:$PASS@$IP/$NAME" 
diesel migration run
```

### Setting up the build

```bash
rustup override set nightly
cargo install cargo-web
```

## Building

Deploy the frontend (continiously). 
This step just compiles the frontend to static wasm/html and puts everything in the `target/deploy/` directory.
```bash
cd frontend

# single deployment
cargo web deploy

# continuous deployment
cargo watch -x "web deploy"
```

To develop the frontend using a custom backend, start the backend server with
```bash
# setup the database for the rocket server
# alternatively, you could write the database url into the Rocket.toml file as well
export ROCKET_DATABASES={postgres_db={url=$DATABASE_URL}}

#requires nightly toolchain
rustup override set nightly

cargo run
```



## Deploying

Warnings: 
- the `uploads/` folder is a volume in the docker container and should not be cleared.

```bash
#change this
export POSTGRES_PASSWORD=yourstrongpasshere
# for further configuration options, see the environment section 
# in the docker-compose.yml file.

# start the service
docker-compose up
```


## References

- [`diesel`](https://diesel.rs)
- [`yew`](https://yew.rs/docs)
- [`postgres`](https://postgresql.org)
- [`docker`](httpsd//www.docker.com)
