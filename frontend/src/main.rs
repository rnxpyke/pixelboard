use yew::format::nothing::Nothing;
use yew::format::Json;
use yew::services::console::*;
use yew::services::fetch::*;
use yew::{html, Component, ComponentLink, Html, ShouldRender};

extern crate failure;

use database::models::Image;

enum Msg {
    Noop,
    Focus(usize),
    FetchResImage(Vec<Image>),
}

struct Model {
    title: String,
    posts: Vec<Image>,
    focus: usize,
    fetch: FetchService,
    link: ComponentLink<Self>,
    task: Option<FetchTask>,
}

impl Model {
    fn fetch_images(&mut self) {
        let get_request = Request::get("images")
            .body(Nothing)
            .expect("Failed to build request.");
        let json_task = self.fetch.fetch(
            get_request,
            self.link.callback(
                |response: Response<Json<Result<Vec<Image>, failure::Error>>>| {
                    let (meta, Json(body)) = response.into_parts();
                    ConsoleService::new().log(&format!("{:#?}", meta));
                    if meta.status.is_success() {
                        return Msg::FetchResImage(body.expect("Nice"));
                    }
                    Msg::Noop
                },
            ),
        );
        self.task = Some(json_task);
    }
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        let mut comp = Self {
            title: "Pixelboard".into(),
            posts: vec![],
            focus: 0,
            fetch: FetchService::new(),
            link: link.clone(),
            task: None,
        };
        comp.fetch_images();
        comp
    }
    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Self::Message::FetchResImage(mut s) => {
                self.posts.append(&mut s);
                true
            }
            Self::Message::Focus(k) => {
                if self.focus != k {
                    self.focus = k;
                    true
                } else {
                    false
                }
            }
            _ => false,
        }
    }
    fn view(&self) -> Html {
        let display_stream = |(k, i): (usize, &Image)| {
            let src = format!("uploads/{}", i.id);
            let timestamp = format!("{}", i.created);
            let class = if self.focus == k {
                "stream-focus"
            } else {
                "stream-thumb"
            };
            html! {
                <div class={class}>
                    <img src=src onclick={ self.link.callback(move |_| Msg::Focus(k)) }/>
                    <p>{ timestamp}</p>
                </div>
            }
        };
        html! {
            <>
            <div class="header">
                <p class="title">{ self.title.clone() }</p>
                <form class="upload-form" action="/uploadForm" method="post" enctype="multipart/form-data">
                    <fieldset>
                    <input name="file" type="file" accept="image/*" />
                    <input type="submit" value="upload" />
                    </fieldset>
                </form>
            </div>
            <div class="stream-container">
            { for self.posts.iter().enumerate().map(display_stream) }
            </div>
            </>
        }
    }
}

fn main() {
    yew::start_app::<Model>();
}
