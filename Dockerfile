FROM rust:latest AS build-deps

RUN rustup install nightly
WORKDIR /usr/src/pixelboard
RUN rustup override set nightly

RUN rustup target add wasm32-unknown-unknown
RUN cargo +nightly install cargo-web

COPY . .

RUN cargo +nightly install --path backend --debug -Z no-index-update
RUN cd frontend; cargo +nightly -Z no-index-update web deploy
RUN cargo +nightly -Z no-index-update install diesel_cli --no-default-features --features postgres

CMD diesel migration run ; backend
