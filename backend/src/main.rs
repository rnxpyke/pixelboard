#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

extern crate diesel;
extern crate serde;

extern crate database;
use database::{models, schema};

use diesel::prelude::*;
use models::*;

use multipart::server::Multipart;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;

use rocket::http::ContentType;
use rocket::Data;
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;

//hack for workspace
macro_rules! WORKSPACE {
    () => {
        concat!(env!("CARGO_MANIFEST_DIR"), "/..")
    };
}

#[database("postgres_db")]
struct DataDbConn(diesel::PgConnection);

mod session {
    //! This is the Session module for the pixelboard backend
    //! When everything is implemented, a session can be established
    //! simply by having a UserSession Request guard.
    //! The UserSession should contain the UserId.

    //! login/logout routes can be enabled with all_session_routes

    use rocket::http::{Cookie, Cookies};
    use rocket::outcome::IntoOutcome;
    use rocket::request::*;
    use rocket::response::{Flash, Redirect};
    use rocket::Route;

    #[derive(Debug)]
    pub struct UserSession(pub usize);

    #[derive(Debug, FromForm)]
    pub struct Login {
        pub username: String,
        pub password: String,
    }


    const LOGIN_PAGE: &'static str = "login";

    pub fn all_session_routes() -> Vec<Route> {
        routes![login, login_user, logout]
    }

    impl<'a, 'r> FromRequest<'a, 'r> for UserSession {
        type Error = std::convert::Infallible;

        fn from_request(request: &'a Request<'r>) -> Outcome<UserSession, Self::Error> {
            request
                .cookies()
                .get_private("user_id")
                .and_then(|cookie| cookie.value().parse().ok())
                .map(|id| UserSession(id))
                .or_forward(())
        }
    }

    #[post("/logout")]
    fn logout(mut cookies: Cookies<'_>) -> Flash<Redirect> {
        cookies.remove_private(Cookie::named("user_id"));
        Flash::success(Redirect::to(LOGIN_PAGE), "Successfully logged out.")
    }

    ///TODO: make database request or something
    #[post("/login", data = "<login>")]
    fn login(mut cookies: Cookies<'_>, login: Form<Login>) -> Result<Redirect, Flash<Redirect>> {
        if login.username == "Test" && login.password == "password" {
            cookies.add_private(Cookie::new("user_id", 1.to_string()));
            Ok(Redirect::to("index.html"))
        } else {
            Err(Flash::error(
                Redirect::to(LOGIN_PAGE),
                "Invalid username/password.",
            ))
        }
    }

    #[get("/login")]
    fn login_user(_user: UserSession) -> Redirect {
        Redirect::to("index.html")
    }
}

#[get("/posts")]
fn get_posts(conn: DataDbConn) -> Json<Vec<models::Post>> {
    use schema::posts::dsl::*;
    Json(posts.load::<Post>(&*conn).expect("Error loading posts"))
}

#[get("/images")]
fn get_images(conn: DataDbConn) -> Json<Vec<models::Image>> {
    use schema::images::dsl::*;
    Json(
        images
            .order(created.desc())
            .load::<Image>(&*conn)
            .expect("Error loading images"),
    )
}

/// Accepts a file path and a data stream and saves the data as a file
/// as long as it is a picture.
fn data_to_file(path: &str, img: &mut dyn Read) -> Result<(), Box<dyn Error>> {
    std::io::copy(img, &mut File::create(&path)?)?;
    let mut buffer = [0; 64];
    {
        let mut f = File::open(&path)?;
        f.read(&mut buffer)?;
    }
    match image::guess_format(&buffer) {
        Ok(_) => Ok(()),
        Err(err) => {
            std::fs::remove_file(path)?;
            Err(err.into())
        }
    }
}

/// This is still very buggy because it doesn't verify the multipart message
#[post("/uploadForm", data = "<input>")]
fn image_upload(
    content_type: &ContentType,
    conn: DataDbConn,
    input: Data,
) -> Result<(), Box<dyn Error>> {
    if !content_type.is_form_data() {
        return Err("Content is not multipart data".into());
    }
    let (_, boundary) = content_type
        .params()
        .find(|&(k, _)| k == "boundary")
        .ok_or_else::<Box<dyn Error>, _>(|| {
            "`Content-Type: multipart/form-data` boundary param not provided".into()
        })?;
    let mut mp = Multipart::with_body(input.open(), boundary);
    if let Ok(Some(mut entry)) = mp.read_entry() {
        upload_transaction(conn, &mut entry.data)
    } else {
        Err("Could not read multipart entry".into())
    }
}

fn upload_transaction(conn: DataDbConn, img: &mut dyn Read) -> Result<(), Box<dyn Error>> {
    use schema::images;
    use schema::images::dsl::*;
    (*conn).transaction(|| {
        let inserted_img = diesel::insert_into(images::table)
            .default_values()
            .execute(&*conn)?;
        let image: Image = images
            .order(id.desc())
            .limit(inserted_img as i64)
            .load(&*conn)?
            .into_iter()
            .next()
            .unwrap();
        let file = format!(concat!(WORKSPACE!(), "/uploads/{id}"), id = image.id);
        data_to_file(&file, img)
    })
}

///TODO: refactor so that the id isn't incremented if data_to_file fails
#[post("/upload", data = "<img>")]
fn upload(conn: DataDbConn, img: Data) -> Result<(), Box<dyn Error>> {
    upload_transaction(conn, &mut img.open())
}

#[get("/create/<title>")]
fn create_post(conn: DataDbConn, title: String) -> Json<Post> {
    use schema::posts;
    let new_post = NewPost {
        title: &title,
        body: "",
    };
    Json(
        diesel::insert_into(posts::table)
            .values(&new_post)
            .get_result::<Post>(&*conn)
            .expect("Error on inserting stuff"),
    )
}

fn main() {
    println!("Serving static Files from: {}", WORKSPACE!());
    rocket::ignite()
        .attach(DataDbConn::fairing())
        .mount(
            "/",
            routes![get_posts, get_images, create_post, upload, image_upload],
        ).mount(
            "/",
            session::all_session_routes(),
        ).mount(
            "/uploads",
            StaticFiles::from(concat!(WORKSPACE!(), "/uploads")).rank(9),
        ).mount(
            "/",
            StaticFiles::from(concat!(WORKSPACE!(), "/target/deploy")).rank(10),
        ).launch();
}
