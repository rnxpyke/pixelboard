#[cfg(feature = "diesel")]
#[macro_use]
extern crate diesel;

extern crate serde;
#[macro_use]
extern crate serde_derive;

pub mod models;
#[cfg(feature = "diesel")]
pub mod schema;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
