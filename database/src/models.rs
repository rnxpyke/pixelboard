#[cfg(feature = "diesel")]
use super::schema::posts;

#[derive(Serialize, Deserialize)]
#[cfg_attr(feature = "diesel", derive(Queryable))]
pub struct Post {
    pub id: i32,
    pub title: String,
    pub body: String,
    pub published: bool,
}

#[derive(Serialize, Deserialize)]
#[cfg_attr(feature = "diesel", derive(Insertable))]
#[cfg_attr(feature = "diesel", table_name = "posts")]
pub struct NewPost<'a> {
    pub title: &'a str,
    pub body: &'a str,
}

#[derive(Serialize, Deserialize)]
#[cfg_attr(feature = "diesel", derive(Queryable))]
pub struct Image {
    pub id: i32,
    pub created: chrono::NaiveDateTime,
}
